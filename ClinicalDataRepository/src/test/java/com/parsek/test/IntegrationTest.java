package com.parsek.test;

import com.parsek.DTO.in.PatientDTOin;
import com.parsek.DTO.out.PatientDTOOut;
import com.parsek.DTO.out.PatientDetailsDTOOut;
import com.parsek.ejb.IPatient;
import com.parsek.ejb.PatientEJB;
import com.parsek.entity.Patient;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.ejb.EJB;
import javax.inject.Inject;
import java.io.File;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RunWith(Arquillian.class)
public class IntegrationTest {


    protected static final String[] DEPENDENCIES = {
            "org.modelmapper:modelmapper:2.3.8",

    };

    @Deployment
    public static WebArchive createDeployment() {

        return ShrinkWrap.create(WebArchive.class,"test.war")
                .addPackages(true, "com.parsek")
                .addPackages(true, "org.modelmapper")
                .addAsWebInfResource("WEB-INF/beans.xml", ArchivePaths.create("beans.xml"))
                .addAsResource(new File("src/test/resources/persistence.xml"), "META-INF/persistence.xml")
                .addAsResource("arquillian.xml", "arquillian.xml")
                .addAsLibraries(Maven.resolver().resolve("org.modelmapper:modelmapper:2.3.8").withTransitivity().asFile());
//                .merge(thirdPartyLibs());
    }

    protected static WebArchive thirdPartyLibs() {
        WebArchive lib = ShrinkWrap.create(WebArchive.class);
        for (String dependency : DEPENDENCIES) {
            lib.merge(Maven.resolver().resolve(dependency).withoutTransitivity().asSingle(JavaArchive.class));
        }
        return lib;
    }

    @EJB
    private IPatient iPatient;

    private static final String name = "TestName";
    private static final String lastName = "TestName";
    private static Date birthday;

    {
        try {
            birthday = new SimpleDateFormat("dd.MM.yyyy").parse("1.1.1990");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void doDummyInsertTest() {
        PatientDTOin patientDTOin = new PatientDTOin();
        patientDTOin.setName(name);
        patientDTOin.setLastName(lastName);
        patientDTOin.setBirthDay(new Timestamp(birthday.getTime()));
        iPatient.persistPatient(patientDTOin);

        List<PatientDTOOut> patientDTOOutList = iPatient.getAllPatient();

        Assert.assertEquals(1, patientDTOOutList.size());
        Assert.assertNotNull(patientDTOOutList.get(0));

        PatientDTOOut patientDTOOut = patientDTOOutList.get(0);
        Assert.assertEquals(patientDTOOut.getName(), name);
        Assert.assertEquals(patientDTOOut.getLastName(), lastName);
        Assert.assertEquals(patientDTOOut.getBirthDay().getTime(), birthday.getTime());

    }

}
