package com.parsek.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Data
@NamedQueries({
        @NamedQuery(name = "BodyTemperature.getTemperatureForPatient", query = "SELECT t FROM BodyTemperature t WHERE t.patient.id =:id ORDER BY t.measurementAt desc "),
        @NamedQuery(name = "BodyTemperature.getMaxForPatients", query = "SELECT p, MAX(t.temperature)  FROM Patient p INNER JOIN p.bodyTemperatureList as t GROUP BY p ")
})
public class BodyTemperature {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @ManyToOne
    private Patient patient;

    private Timestamp measurementAt;
    private Float temperature;
}
