package com.parsek.entity;

import lombok.Data;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;

@Entity
@Data
@NamedQueries({@NamedQuery(name = "Patient.search", query = "SELECT p FROM Patient  p WHERE lower( p.name) LIKE concat( lower( :searchString),'%')" +
        "or lower( p.lastName) LIKE concat( lower( :searchString),'%')" ),
        @NamedQuery(name="Patient.idWithDetails",query = "SELECT p FROM Patient p left join fetch p.bodyTemperatureList WHERE p.id = :id")
})
public class Patient {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;
    private String lastName;
    private Date birthDay;

    @OneToMany(mappedBy = "patient")
    private List<BodyTemperature> bodyTemperatureList;

}
