package com.parsek.repository;

import com.parsek.entity.BodyTemperature;
import com.parsek.entity.Patient;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Timestamp;

@RequestScoped
public class BodyTemperatureRepository {
    @PersistenceContext
    private EntityManager em;

    public void persistBodyTemperature(Long patientId, Float bodyTemperature, Timestamp measurementAt) {
        Patient patient = em.find(Patient.class, patientId);
        BodyTemperature bodyTemperature1 = new BodyTemperature();
        bodyTemperature1.setPatient(patient);
        bodyTemperature1.setMeasurementAt(measurementAt);
        bodyTemperature1.setTemperature(bodyTemperature);
        em.persist(bodyTemperature1);
    }
}
