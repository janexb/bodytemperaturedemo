package com.parsek.repository;

import com.parsek.entity.BodyTemperature;
import com.parsek.entity.Patient;

import javax.enterprise.context.RequestScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.ArrayList;
import java.util.List;

@RequestScoped
public class PatientRepository {
    @PersistenceContext
    private EntityManager em;

    public void persistPatient(Patient patient) {

        em.persist(patient);
    }

    public List<Patient> getAllPatient() {
        List<Patient> rez = em.createQuery("SELECT p FROM Patient p").getResultList();
        return rez;
    }

    public Patient getPatient(Long id) {
        Patient patient = em.find(Patient.class, id);
        if (patient== null) {
            return null;
        }
        return patient;
    }

    public void persistBodyTemperature(BodyTemperature bodyTemperature) {
        em.persist(bodyTemperature);
    }

    public List<BodyTemperature> getBodyTemperaturePatient(long patientId) {
        return em.createNamedQuery("BodyTemperature.getTemperatureForPatient").setParameter("id", patientId)
                .getResultList();
    }

    public List<Patient> getPatientSearch(String searchString) {
        return em.createNamedQuery("Patient.search").setParameter("searchString", searchString).getResultList();
    }

    public List<Patient> getMaxTemperaturesPErPatient() {
        List<Patient> patientList = new ArrayList<>();
        List<Object[]> rez = em.createNamedQuery("BodyTemperature.getMaxForPatients").getResultList();
        for (Object o[] : rez) {
            Patient p = (Patient) o[0];

            Float maxTemp = (Float) o[1];

            p.getBodyTemperatureList().clear();
            BodyTemperature bodyTemperature = new BodyTemperature();
            bodyTemperature.setTemperature(maxTemp);
            p.getBodyTemperatureList().add(bodyTemperature);

            patientList.add(p);
        }
        return patientList;
    }

    public Patient getPatientWithDetails(Long id) {
        return em.createNamedQuery("Patient.idWithDetails", Patient.class).setParameter("id", id).getSingleResult();
    }
}
