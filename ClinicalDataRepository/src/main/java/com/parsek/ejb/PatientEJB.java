package com.parsek.ejb;

import com.parsek.DTO.in.BodyTemperatureMeasurementsDTOIn;
import com.parsek.DTO.in.PatientDTOin;
import com.parsek.DTO.out.*;
import com.parsek.entity.BodyTemperature;
import com.parsek.entity.Patient;
import com.parsek.repository.PatientRepository;
import com.parsek.utils.ObjectMapperUtils;
import org.modelmapper.ModelMapper;
import org.modelmapper.convention.MatchingStrategies;

import javax.annotation.PostConstruct;
import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

@Stateless
@Remote(IPatient.class)
public class PatientEJB implements IPatient {


    private ModelMapper modelMapper;

    @Inject
    private PatientRepository patientRepository;

    @PostConstruct
    private void init() {
        modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setMatchingStrategy(MatchingStrategies.STRICT);
    }

    @Override
    public PatientDTOOut getPatient(Long id) {
        Patient patient = patientRepository.getPatient(id);
        if (patient == null) {
            return null;
        }
        return modelMapper.map(patientRepository.getPatient(id), PatientDTOOut.class);
    }

    @Override
    public PatientDTOOut persistPatient(PatientDTOin patientDTOin) {

        Patient patient = modelMapper.map(patientDTOin, Patient.class);
        patientRepository.persistPatient(patient);

        PatientDTOOut patientDTOOut = modelMapper.map(patient, PatientDTOOut.class);


        return patientDTOOut;
    }

    @Override
    public List<PatientDTOOut> getAllPatient() {
        List<Patient> patients = patientRepository.getAllPatient();
        List<PatientDTOOut> patientDTOOutList = ObjectMapperUtils.mapAll(patients, PatientDTOOut.class);
        return patientDTOOutList;
    }

    @Override
    public PatientDetailsDTOOut getPatientsDetails(Long id) {

        Patient patient = patientRepository.getPatient(id);
        if (patient != null) {
            PatientDetailsDTOOut patientDetailsDTOOut = new PatientDetailsDTOOut();
            patientDetailsDTOOut.setPatientDTOOut(modelMapper.map(patient, PatientDTOOut.class));
            patientDetailsDTOOut.setTemperatureList(new ArrayList<>());

            return patientDetailsDTOOut;
        } else {
            return null;
        }

    }

    @Override
    public void persistPatientBodyTemperature(BodyTemperatureMeasurementsDTOIn bodyTemperatureMeasurementsDTOIn) {
        Patient patient = patientRepository.getPatient(bodyTemperatureMeasurementsDTOIn.getIdPatient());
        BodyTemperature bodyTemperature = new BodyTemperature();
        bodyTemperature.setPatient(patient);
        bodyTemperature.setTemperature(bodyTemperatureMeasurementsDTOIn.getTemperature());
        bodyTemperature.setMeasurementAt(bodyTemperatureMeasurementsDTOIn.getMeasurementAt());

        patientRepository.persistBodyTemperature(bodyTemperature);

    }

    @Override
    public ListBodyTemperaturePatientOut getTemperatureForPatient(long patientId) {
        List<BodyTemperature> bodyTemperatures = patientRepository.getBodyTemperaturePatient(patientId);
        ListBodyTemperaturePatientOut listBodyTemperaturePatientOut = new ListBodyTemperaturePatientOut();
        bodyTemperatures.stream().forEach(bodyTemperature -> {
            BodyTemperatureMeasurementsDTOOut bodyTemperatureMeasurementsDTOOut = new BodyTemperatureMeasurementsDTOOut();
            bodyTemperatureMeasurementsDTOOut.setId(bodyTemperature.getId());
            bodyTemperatureMeasurementsDTOOut.setIdPatient(patientId);
            bodyTemperatureMeasurementsDTOOut.setMeasurementAt(bodyTemperature.getMeasurementAt());
            bodyTemperatureMeasurementsDTOOut.setTemperature(bodyTemperature.getTemperature());

            listBodyTemperaturePatientOut.getTemperaturesList().add(bodyTemperatureMeasurementsDTOOut);
        });
        return listBodyTemperaturePatientOut;
    }

    @Override
    public List<PatientDTOOut> getPatientSearch(String searchString) {
        List<Patient> patients = patientRepository.getPatientSearch(searchString);
        List<PatientDTOOut> patientDTOOutList = ObjectMapperUtils.mapAll(patients, PatientDTOOut.class);
        return patientDTOOutList;
    }

    @Override
    public ListMaxTemperaturePerPatientsDTOOut getExportMaxTemperaturePatients() {
        List<Patient> bodyTemperatures = patientRepository.getMaxTemperaturesPErPatient();

        ListMaxTemperaturePerPatientsDTOOut rez = new ListMaxTemperaturePerPatientsDTOOut();

        bodyTemperatures.stream().forEach(patient -> {
            MaxTemperaturePatientDTOOut maxTemperaturePatientDTOOut = new MaxTemperaturePatientDTOOut();
            maxTemperaturePatientDTOOut.setName(patient.getName() + " " + patient.getLastName());
            maxTemperaturePatientDTOOut.setMaxTemperature(patient.getBodyTemperatureList().get(0).getTemperature());
            rez.getPatientsMAxTemperature().add(maxTemperaturePatientDTOOut);
        });

        return rez;
    }

    @Override
    public PatientDetailsDTOOut getPatientWithDetails(Long id) {
        PatientDetailsDTOOut patientDetailsDTOOut = new PatientDetailsDTOOut();

        Patient patient = patientRepository.getPatientWithDetails(id);

        patientDetailsDTOOut.setPatientDTOOut(modelMapper.map(patient, PatientDTOOut.class));
        patient.getBodyTemperatureList().stream().forEach(bodyTemperature -> {
            BodyTemperatureMeasurementsDTOOut bodyTemperatureMeasurementsDTOOut = new BodyTemperatureMeasurementsDTOOut();
            bodyTemperatureMeasurementsDTOOut.setId(bodyTemperature.getId());
            bodyTemperatureMeasurementsDTOOut.setTemperature(bodyTemperature.getTemperature());
            bodyTemperatureMeasurementsDTOOut.setMeasurementAt(bodyTemperature.getMeasurementAt());
            patientDetailsDTOOut.getTemperatureList().add(bodyTemperatureMeasurementsDTOOut);
        });
        return patientDetailsDTOOut;
    }
}
