package com.parsek.ejb;

import com.parsek.repository.BodyTemperatureRepository;

import javax.ejb.Remote;
import javax.ejb.Stateless;
import javax.inject.Inject;
import java.sql.Timestamp;

@Stateless
@Remote(IBodyTemperature.class)
public class BodyTemperatureEJB implements IBodyTemperature {

    @Inject
    private BodyTemperatureRepository bodyTemperatureRepository;

    @Override
    public void persistBodyTemperature(Long patientId, Float bodyTemperature, Timestamp measurementAt) {
        bodyTemperatureRepository.persistBodyTemperature(patientId, bodyTemperature, measurementAt);
    }
}
