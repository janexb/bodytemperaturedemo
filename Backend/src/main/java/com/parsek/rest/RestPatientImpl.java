package com.parsek.rest;

import com.parsek.DTO.in.BodyTemperatureMeasurementsDTOIn;
import com.parsek.DTO.in.PatientDTOin;
import com.parsek.DTO.out.AllPatientsDTOOut;
import com.parsek.DTO.out.ListBodyTemperaturePatientOut;
import com.parsek.DTO.out.ListMaxTemperaturePerPatientsDTOOut;
import com.parsek.DTO.out.PatientDetailsDTOOut;
import com.parsek.proxy.PatientProxy;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.ws.rs.core.Response;

@RequestScoped
public class RestPatientImpl implements IRestPatient {

    @Inject
    private PatientProxy patientProxy;


    @Override
    public Response persistPatient(PatientDTOin in) {

        patientProxy.persistPatient(in);
        return Response.ok().build();
    }

    @Override
    public Response getAllPatients() {
        AllPatientsDTOOut allPatientsDTOOut = new AllPatientsDTOOut();
        allPatientsDTOOut.setPatients(patientProxy.getAllPatients());
        return Response.ok().entity(allPatientsDTOOut).build();
    }

    @Override
    public Response getPatientDetail(String id) {

        PatientDetailsDTOOut patientDetailsDTOOut = patientProxy.getPatientsDetails(Long.parseLong(id));

        if (patientDetailsDTOOut!= null) {
            return Response.ok().entity(patientDetailsDTOOut).build();
        } else {
            return Response.status(404).build();
        }

    }

    @Override
    public Response saveTemperature(BodyTemperatureMeasurementsDTOIn bodyTemperatureMeasurementsDTOIn) {
        patientProxy.persistBodyTemperature(bodyTemperatureMeasurementsDTOIn);
        return Response.ok().build();
    }

    @Override
    public Response getTemperaturePatient(String id) {
        ListBodyTemperaturePatientOut listBodyTemperaturePatientOut = patientProxy.getTemperatureForPatient(Long.parseLong(id));
        return Response.ok().entity(listBodyTemperaturePatientOut).build();
    }

    @Override
    public Response getPatientsSearch(String searchString) {
        AllPatientsDTOOut allPatientsDTOOut = new AllPatientsDTOOut();
        allPatientsDTOOut.setPatients(patientProxy.getPatientsSearch(searchString));
        return Response.ok().entity(allPatientsDTOOut).build();
    }


    @Override
    public Response getMaxTemperature() {
        ListMaxTemperaturePerPatientsDTOOut listMaxTemperaturePerPatientsDTOOut = patientProxy.getExportMaxTemperaturePatients();

        return Response.ok().entity(listMaxTemperaturePerPatientsDTOOut).build();
    }
}
