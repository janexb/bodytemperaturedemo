package com.parsek.fhir.server;

import ca.uhn.fhir.context.FhirContext;
import ca.uhn.fhir.rest.server.IResourceProvider;
import ca.uhn.fhir.rest.server.RestfulServer;
import com.parsek.fhir.provider.FHIRObservationsResourceProviderImpl;
import com.parsek.fhir.provider.FHIRPatientResourceProviderImpl;
import com.parsek.proxy.PatientProxy;

import javax.inject.Inject;
import javax.servlet.annotation.WebServlet;
import java.util.ArrayList;
import java.util.List;
@WebServlet(urlPatterns= {"/fhir/*"}, displayName="FHIR Server")
public class FhirRestfulServlet extends RestfulServer {

    @Inject
    private PatientProxy patientProxy;

    public FhirRestfulServlet() {
        super(FhirContext.forR4());
    }



    @Override
    public void initialize() {

        List<IResourceProvider> providers = new ArrayList<>();
        providers.add(new FHIRPatientResourceProviderImpl());
        providers.add(new FHIRObservationsResourceProviderImpl());

        setResourceProviders(providers);


    }

}
