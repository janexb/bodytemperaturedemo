package com.parsek.fhir.provider;

import ca.uhn.fhir.model.primitive.UriDt;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;
import com.parsek.DTO.in.PatientDTOin;
import com.parsek.DTO.out.PatientDTOOut;
import com.parsek.fhir.IFHIRPatient;
import com.parsek.proxy.PatientProxy;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.StringType;

import javax.enterprise.inject.spi.CDI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class FHIRPatientResourceProviderImpl implements IFHIRPatient, IResourceProvider {

    private PatientProxy patientProxy;

    public FHIRPatientResourceProviderImpl() {
        patientProxy = CDI.current().select(PatientProxy.class).get();
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Patient.class;
    }


    @Override
    public List<Patient> getAllPatients() {
        List<PatientDTOOut> patientDTOOutList = patientProxy.getAllPatients();
        List<Patient> patients = new ArrayList<>();
        patientDTOOutList.stream().forEach(patientDTOOut -> {
            Patient patient = getPatient(patientDTOOut);

            patients.add(patient);

        });
        return patients;
    }

    @Override
    public Patient getPatientById(IdType theId) {

        PatientDTOOut patientDTOOut = patientProxy.getPatient(theId.getIdPartAsLong());
        if (patientDTOOut != null) {
            return getPatient(patientDTOOut);
        } else {
            return null;
        }


    }


    @Override
    public MethodOutcome createPatient(Patient patient) {
        PatientDTOin patientDTOin = new PatientDTOin();
        patientDTOin.setBirthDay(new Timestamp(patient.getBirthDate().getTime()));
        patientDTOin.setName(patient.getName().get(0).getGiven().get(0).getValue());
        patientDTOin.setLastName(patient.getName().get(1).getFamily());
        patientProxy.persistPatient(patientDTOin);
        MethodOutcome retVal = new MethodOutcome();

        return retVal;
    }

    @Override
    public List<Patient> searchPatients(StringType stringType) {
        List<PatientDTOOut> patientDTOOuts = patientProxy.getPatientsSearch(stringType.getValue());
        List<Patient> patients = new ArrayList<>();
        patientDTOOuts.stream().forEach(patientDTOOut -> {
            patients.add(getPatient(patientDTOOut));
        });
        return patients;
    }

    public static Patient getPatient(PatientDTOOut patientDTOOut) {
        Patient patient = new Patient();

        patient.setId(patientDTOOut.getId().toString());
        patient.addIdentifier();
        patient.getIdentifier().get(0).setSystem(String.valueOf(new UriDt("urn:hapitest:mrns")));
        patient.getIdentifier().get(0).setValue(patientDTOOut.getId().toString());
        patient.addIdentifier().setValue(patientDTOOut.getId().toString());

        patient.addName().addGiven(patientDTOOut.getName());
        patient.addName().setFamily(patientDTOOut.getLastName());

        patient.setBirthDate(new Date(patientDTOOut.getBirthDay().getTime()));
        return patient;
    }
}
