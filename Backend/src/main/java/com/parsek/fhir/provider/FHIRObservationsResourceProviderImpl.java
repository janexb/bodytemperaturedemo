package com.parsek.fhir.provider;

import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.server.IResourceProvider;
import com.parsek.DTO.in.BodyTemperatureMeasurementsDTOIn;
import com.parsek.DTO.out.PatientDetailsDTOOut;
import com.parsek.fhir.IFHIRObservation;
import com.parsek.proxy.PatientProxy;
import org.hl7.fhir.instance.model.api.IBaseResource;
import org.hl7.fhir.r4.model.*;

import javax.enterprise.inject.spi.CDI;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class FHIRObservationsResourceProviderImpl implements IFHIRObservation, IResourceProvider {
    private PatientProxy patientProxy;

    public FHIRObservationsResourceProviderImpl() {
        patientProxy = CDI.current().select(PatientProxy.class).get();
    }

    @Override
    public Class<? extends IBaseResource> getResourceType() {
        return Observation.class;
    }

    @Override
    public MethodOutcome createObservation(Observation observation) {


        BodyTemperatureMeasurementsDTOIn bodyTemperatureMeasurementsDTOIn = getBodyTemperatureMeasurementsDTOIn(observation);
        patientProxy.persistBodyTemperature(bodyTemperatureMeasurementsDTOIn);

        MethodOutcome retVal = new MethodOutcome();

        return retVal;
    }

    private BodyTemperatureMeasurementsDTOIn getBodyTemperatureMeasurementsDTOIn(Observation observation) {
        BodyTemperatureMeasurementsDTOIn bodyTemperatureMeasurementsDTOIn = new BodyTemperatureMeasurementsDTOIn();
        bodyTemperatureMeasurementsDTOIn.setMeasurementAt(new Timestamp(((DateTimeType) observation.getEffective()).getValue().getTime()));
        bodyTemperatureMeasurementsDTOIn.setIdPatient(observation.getSubject().getReferenceElement().getIdPartAsLong());
        bodyTemperatureMeasurementsDTOIn.setTemperature(((Quantity) observation.getValue()).getValue().floatValue());
        return bodyTemperatureMeasurementsDTOIn;
    }

    @Override
    public List<Observation> getObservationsForPatient(StringType patientId) {
        PatientDetailsDTOOut patientDetailsDTOOut = patientProxy.getPatientWithDetails(Long.parseLong(patientId.getValue()));
        List<Observation> rez = getObservations(patientDetailsDTOOut);
        return rez;
    }

    public static List<Observation> getObservations(PatientDetailsDTOOut patientDetailsDTOOut) {
        List<Observation> rez = new ArrayList<>();

        Patient patient = FHIRPatientResourceProviderImpl.getPatient(patientDetailsDTOOut.getPatientDTOOut());

        patientDetailsDTOOut.getTemperatureList().stream().forEach(bodyTemperatureMeasurementsDTOOut -> {

            Observation observation = new Observation();
            observation.setId(bodyTemperatureMeasurementsDTOOut.getId().toString());
            Quantity quantity = new Quantity();
            quantity.setValue(new java.math.BigDecimal(bodyTemperatureMeasurementsDTOOut.getTemperature().toString()));
            quantity.setUnit("Cel");
            observation.setValue(quantity);


            observation.setSubject(new Reference(patient));
            observation.setEffective(new DateTimeType(bodyTemperatureMeasurementsDTOOut.getMeasurementAt()));
            Coding coding = new Coding("http://loinc.org", "8310-5", "Body temperature");

            observation.setCode((new CodeableConcept()).addCoding(coding));

            rez.add(observation);
        });

        return rez;
    }
}
