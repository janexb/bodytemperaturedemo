package com.parsek.servlet;

import com.parsek.DTO.out.ListMaxTemperaturePerPatientsDTOOut;
import com.parsek.proxy.PatientProxy;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.Calendar;

@WebServlet("/export")
public class UploadPatientExport extends HttpServlet {

    @Inject
    private PatientProxy patientProxy;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("application/octet-stream");
        String headerKey = "Content-Disposition";
        Calendar currentDate = Calendar.getInstance();
        String headerValue = String.format("attachment; filename=export_" + currentDate.get(Calendar.YEAR) + "-" + currentDate.get(Calendar.MONTH) + "-" + currentDate.get(Calendar.DAY_OF_MONTH) + "_" + currentDate.get(Calendar.HOUR) + "-" + currentDate.get(Calendar.MINUTE) + ".json");
        resp.setHeader(headerKey, headerValue);
        OutputStream os = resp.getOutputStream();
        OutputStreamWriter writer = new OutputStreamWriter(os);
        ListMaxTemperaturePerPatientsDTOOut listMaxTemperaturePerPatientsDTOOut = patientProxy.getExportMaxTemperaturePatients();
        JSONArray jsonArray = new JSONArray();
        listMaxTemperaturePerPatientsDTOOut.getPatientsMAxTemperature().stream().forEach(maxTemperaturePatientDTOOut -> {
            JSONObject jsonObject = new JSONObject();
            jsonArray.add(jsonObject);

            jsonObject.put("maxTemperature", maxTemperaturePatientDTOOut.getMaxTemperature());
            jsonObject.put("name", maxTemperaturePatientDTOOut.getName());
        });
        jsonArray.writeJSONString(writer);
        writer.close();
        os.close();
    }
}
