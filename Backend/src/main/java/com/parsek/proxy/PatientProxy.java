package com.parsek.proxy;

import com.parsek.DTO.in.BodyTemperatureMeasurementsDTOIn;
import com.parsek.DTO.in.PatientDTOin;
import com.parsek.DTO.out.ListBodyTemperaturePatientOut;
import com.parsek.DTO.out.ListMaxTemperaturePerPatientsDTOOut;
import com.parsek.DTO.out.PatientDTOOut;
import com.parsek.DTO.out.PatientDetailsDTOOut;
import com.parsek.ejb.IPatient;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import java.util.List;

@RequestScoped
public class PatientProxy {
    @EJB(mappedName = "ejb:/ClinicalDataRepository/PatientEJB!com.parsek.ejb.IPatient")
    private IPatient iPatient;

    public void persistPatient(PatientDTOin patientDTOin) {
        iPatient.persistPatient(patientDTOin);
    }

    public List<PatientDTOOut> getAllPatients() {
        return iPatient.getAllPatient();
    }

    public PatientDetailsDTOOut getPatientsDetails(Long id) {
        return iPatient.getPatientsDetails(id);
    }

    public void persistBodyTemperature(BodyTemperatureMeasurementsDTOIn bodyTemperatureMeasurementsDTOIn) {
        iPatient.persistPatientBodyTemperature(bodyTemperatureMeasurementsDTOIn);
    }

    public ListBodyTemperaturePatientOut getTemperatureForPatient(long patientId) {
        return iPatient.getTemperatureForPatient(patientId);
    }

    public List<PatientDTOOut> getPatientsSearch(String searchString) {
        return iPatient.getPatientSearch(searchString);
    }

    public ListMaxTemperaturePerPatientsDTOOut getExportMaxTemperaturePatients() {
        return iPatient.getExportMaxTemperaturePatients();
    }

    public PatientDetailsDTOOut getPatientWithDetails(Long id) {
        return iPatient.getPatientWithDetails(id);
    }

    public PatientDTOOut getPatient(Long id) {
        return iPatient.getPatient(id);
    }
}
