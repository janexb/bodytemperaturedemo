package com.parsek.test.fhir;

import com.parsek.DTO.in.PatientDTOin;
import com.parsek.DTO.out.PatientDTOOut;
import com.parsek.fhir.provider.FHIRPatientResourceProviderImpl;
import org.hl7.fhir.r4.model.Patient;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FHIRTest {

    private static final String name = "TestName";
    private static final String lastName = "TestName";
    private static Date birthday;

    {
        try {
            birthday = new SimpleDateFormat("dd.MM.yyyy").parse("1.1.1990");
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void testGetFHIRPatient(){


        PatientDTOOut patientDTOOut = new PatientDTOOut();
        patientDTOOut.setId(1L);
        patientDTOOut.setName(name);
        patientDTOOut.setLastName(lastName);
        patientDTOOut.setBirthDay(new Timestamp(birthday.getTime()));

        Patient patient = FHIRPatientResourceProviderImpl.getPatient(patientDTOOut);

        Assert.assertEquals(patient.getName().get(0).getGiven().get(0).getValue(),name);
        Assert.assertEquals(patient.getName().get(1).getFamily(),lastName);
        Assert.assertEquals(patient.getBirthDate().getTime(),birthday.getTime());


    }
}
