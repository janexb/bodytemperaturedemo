#Project structure: #
The project is composed with several submodules:
* ClinicalDataRepository
* Backend
* Frontend
* SharedObject

#Building project: #
``` 
mvn clean
mvn prepare-package
mvn package
```
#Deploy projects:
## Create database:
```
psql 
create database data;
\q
psql  data < dump.sql
```
deploy driver for postgresql Wildfly:

postgresql-42.2.14.jar

Create datasource with name: **PostgresDS** 

## Deploy projects:
files to deploy on wildfly:

```
ClinicalDataRepository/target/ClinicalDataRepository.war
Backend/target/Backend.war
Frontend/target/Frontend.war
```
# URL:
```
http://localhost:8080/Frontend/FHIRCreatePatient.xhtml

```
