package com.parsek.fhir;

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import ca.uhn.fhir.rest.param.StringParam;
import org.hl7.fhir.r4.model.Identifier;
import org.hl7.fhir.r4.model.Observation;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.StringType;

import java.util.List;

/**
 * Interface for FHIR observation for client and server
 */
public interface IFHIRObservation {
    /**
     * Create observation
     * @param observation
     * @return
     */
    @Create
    MethodOutcome createObservation(@ResourceParam Observation observation);

    /**
     * Get list of observations for particular patient
     * @param patientId
     * @return
     */
    @Search
    List<Observation> getObservationsForPatient(@RequiredParam(name = Patient.SP_IDENTIFIER) StringType patientId);
}
