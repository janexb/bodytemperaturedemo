package com.parsek.fhir;

import ca.uhn.fhir.rest.annotation.*;
import ca.uhn.fhir.rest.api.MethodOutcome;
import org.hl7.fhir.r4.model.IdType;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.StringType;

import java.util.List;

/**
 * Interface for FHIR patients for client and server
 */
public interface IFHIRPatient {

    /**
     * Get all patients
     * @return
     */
    @Search
    List<Patient> getAllPatients();

    /**
     * Create patient
     * @param patient
     * @return
     */
    @Create
    MethodOutcome createPatient(@ResourceParam Patient patient);

    /**
     * Get patient by id
     * @param theId
     * @return
     */
    @Read
    Patient getPatientById(@IdParam IdType theId);

    /**
     * Search for patients with prefix for name and lastname
     * @param stringType
     * @return
     */
    @Search
    List<Patient> searchPatients(@RequiredParam(name = Patient.SP_GIVEN) StringType stringType);
}
