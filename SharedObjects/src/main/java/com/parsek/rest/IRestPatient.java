package com.parsek.rest;

import com.parsek.DTO.in.BodyTemperatureMeasurementsDTOIn;
import com.parsek.DTO.in.PatientDTOin;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Interfece for REST server and client
 */
@Path("patients")
public interface IRestPatient {
    /**
     * Persist patient
     * @param in
     * @return
     */
    @POST
    @Path("add")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response persistPatient(PatientDTOin in);

    /**
     * Get all patients
     * @return
     */
    @GET
    @Path("all")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response getAllPatients();

    /**
     * Get particular patient by id
     * @param id
     * @return
     */
    @GET
    @Path("{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response getPatientDetail(@PathParam("id") String id);

    /**
     * Persist temperature for particular patient
     * @param bodyTemperatureMeasurementsDTOIn
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response saveTemperature(BodyTemperatureMeasurementsDTOIn bodyTemperatureMeasurementsDTOIn);

    /**
     * Get temperature list for particular patient
     * @param id
     * @return
     */
    @GET
    @Path("/temperature/{id}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response getTemperaturePatient(@PathParam("id") String id);

    /**
     * Search for patients with prefix name and lastname
     * @param searchString
     * @return
     */
    @POST
    @Path("/search")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response getPatientsSearch(String searchString);

    /**
     * Get JSON with patients with max temperatures
     * @return
     */
    @GET
    @Path("/getmaxtemperature")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    Response getMaxTemperature();
}
