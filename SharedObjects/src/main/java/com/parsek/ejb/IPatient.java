package com.parsek.ejb;

import com.parsek.DTO.in.BodyTemperatureMeasurementsDTOIn;
import com.parsek.DTO.in.PatientDTOin;
import com.parsek.DTO.out.ListBodyTemperaturePatientOut;
import com.parsek.DTO.out.ListMaxTemperaturePerPatientsDTOOut;
import com.parsek.DTO.out.PatientDTOOut;
import com.parsek.DTO.out.PatientDetailsDTOOut;

import java.util.List;

/**
 * Interface for patient adding, getting and get list of body temperature
 */
public interface IPatient {
    /**
     * Get patient by id
     * @param id
     * @return
     */
    PatientDTOOut getPatient(Long id);

    /**
     * Persist patient
     * @param patientDTOin
     * @return
     */
    PatientDTOOut persistPatient(PatientDTOin patientDTOin);

    /**
     * get all patient
     * @return
     */
    List<PatientDTOOut> getAllPatient();

    /**
     * Get patient with details (temperature list)
     * @param id
     * @return
     */
    PatientDetailsDTOOut getPatientsDetails(Long id);

    /**
     * Persist temperature for particular patient
     * @param bodyTemperatureMeasurementsDTOIn
     */
    void persistPatientBodyTemperature(BodyTemperatureMeasurementsDTOIn bodyTemperatureMeasurementsDTOIn);

    /**
     * List of body temperatures for particular patient
     * @param patientId
     * @return
     */
    ListBodyTemperaturePatientOut getTemperatureForPatient(long patientId);

    /**
     * Search patient with prefix by name and lastname
     * @param searchString
     * @return
     */
    List<PatientDTOOut> getPatientSearch(String searchString);

    /**
     * Get list of all patients with max temperatures
     * @return
     */
    ListMaxTemperaturePerPatientsDTOOut getExportMaxTemperaturePatients();

    /**
     * Get particular patient with details
     * @param id
     * @return
     */
    PatientDetailsDTOOut getPatientWithDetails(Long id);


}
