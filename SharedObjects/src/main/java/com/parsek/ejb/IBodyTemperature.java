package com.parsek.ejb;

import java.sql.Timestamp;

/**
 * Interface for adding body temperature to patient
 * for EJB and client
 */

public interface IBodyTemperature {
    /**
     * Persist Body temperature
     * @param patientId
     * @param bodyTemperature
     * @param measurementAt
     */
    void persistBodyTemperature(Long patientId, Float bodyTemperature, Timestamp measurementAt);
}
