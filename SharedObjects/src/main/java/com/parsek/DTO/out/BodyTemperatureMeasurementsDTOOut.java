package com.parsek.DTO.out;

import com.parsek.DTO.in.BodyTemperatureMeasurementsDTOIn;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = true)
public class BodyTemperatureMeasurementsDTOOut extends BodyTemperatureMeasurementsDTOIn implements Serializable {
    private Long id;
}
