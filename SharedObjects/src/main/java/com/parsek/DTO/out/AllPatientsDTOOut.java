package com.parsek.DTO.out;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class AllPatientsDTOOut implements Serializable {
    private List<PatientDTOOut> patients;
}
