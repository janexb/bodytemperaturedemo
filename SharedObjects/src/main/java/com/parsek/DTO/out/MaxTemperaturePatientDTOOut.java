package com.parsek.DTO.out;

import lombok.Data;

import java.io.Serializable;

@Data
public class MaxTemperaturePatientDTOOut implements Serializable {
    private String name;
    private Float maxTemperature;
}
