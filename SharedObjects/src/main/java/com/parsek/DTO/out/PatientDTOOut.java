package com.parsek.DTO.out;

import com.parsek.DTO.in.PatientDTOin;
import lombok.Data;
import lombok.EqualsAndHashCode;

@Data
@EqualsAndHashCode(callSuper = true)
public class PatientDTOOut extends PatientDTOin {
    private Long id;
}
