package com.parsek.DTO.out;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class ListBodyTemperaturePatientOut implements Serializable {

    Long patientId;
    List<BodyTemperatureMeasurementsDTOOut> temperaturesList = new ArrayList<>();
}
