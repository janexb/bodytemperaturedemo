package com.parsek.DTO.out;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class ListMaxTemperaturePerPatientsDTOOut implements Serializable {
    List<MaxTemperaturePatientDTOOut> patientsMAxTemperature = new ArrayList<>();

}
