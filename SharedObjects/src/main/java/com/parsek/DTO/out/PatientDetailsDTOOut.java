package com.parsek.DTO.out;

import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class PatientDetailsDTOOut implements Serializable {
    private PatientDTOOut patientDTOOut;
    private List<BodyTemperatureMeasurementsDTOOut> temperatureList = new ArrayList<>();
}
