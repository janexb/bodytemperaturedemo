package com.parsek.DTO.in;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class PatientDTOin implements Serializable {
    private String name;
    private String lastName;
    private Timestamp birthDay;

}
