package com.parsek.DTO.in;

import lombok.Data;

import java.io.Serializable;
import java.sql.Timestamp;

@Data
public class BodyTemperatureMeasurementsDTOIn  implements Serializable {
    Long idPatient;
    Timestamp measurementAt;
    Float temperature;
}
