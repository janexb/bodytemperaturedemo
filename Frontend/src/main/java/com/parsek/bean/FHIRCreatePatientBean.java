package com.parsek.bean;

import com.parsek.DTO.out.PatientDTOOut;
import com.parsek.fhir.IFHIRPatientClient;
import lombok.Data;
import org.hl7.fhir.r4.model.Patient;
import org.hl7.fhir.r4.model.StringType;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

@Named("FHIRCreatePatient")
@RequestScoped
public class FHIRCreatePatientBean implements Serializable {

    private List<PatientDTOOut> allPatients = new ArrayList<>();
    private String name;
    private String lastName;
    private Date birthday;

    private String searchString;

    private String urlExport;

    @Inject
    private IFHIRPatientClient ifhirPatientClient;

    @Inject
    private Properties properties;


    public List<PatientDTOOut> getAllPatients() {
        return allPatients;
    }

    public void setAllPatients(List<PatientDTOOut> allPatients) {
        this.allPatients = allPatients;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public String getSearchString() {
        return searchString;
    }

    public void setSearchString(String searchString) {
        this.searchString = searchString;
    }

    public String getUrlExport() {
        return urlExport;
    }

    public void setUrlExport(String urlExport) {
        this.urlExport = urlExport;
    }

    @PostConstruct
    private void init() {
        urlExport = properties.getProperty("backend.export");
        loadData();
    }

    private void loadData() {
        List<Patient> patients = ifhirPatientClient.getAllPatients();
        allPatients.clear();
        setPatients(patients);
    }

    private void setPatients(List<Patient> patients) {
        patients.stream().forEach(patient -> {
            PatientDTOOut patientDTOOut = new PatientDTOOut();
            patientDTOOut.setId(Long.parseLong(patient.getIdentifier().get(0).getValue()));
            patientDTOOut.setName(patient.getName().get(0).getGiven().get(0).getValue());
            patientDTOOut.setLastName(patient.getName().get(1).getFamily());
            patientDTOOut.setBirthDay(new Timestamp(patient.getBirthDate().getTime()));
            allPatients.add(patientDTOOut);
        });
    }

    public void submit() {
        Patient patient = new Patient();
        patient.addName().addGiven(getName());
        patient.addName().setFamily(getLastName());
        patient.setBirthDate(getBirthday());
        ifhirPatientClient.createPatient(patient);
        loadData();
    }

    public void search() {
        allPatients.clear();
        List<Patient> patients = ifhirPatientClient.searchPatients(new StringType(getSearchString()));
        setPatients(patients);
    }
}
