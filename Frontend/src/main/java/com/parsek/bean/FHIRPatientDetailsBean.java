package com.parsek.bean;

import ca.uhn.fhir.rest.server.exceptions.ResourceNotFoundException;
import com.parsek.DTO.out.BodyTemperatureMeasurementsDTOOut;
import com.parsek.fhir.IFHIRObservationClient;
import com.parsek.fhir.IFHIRPatientClient;
import org.hl7.fhir.r4.model.*;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Named("FHIRPatientDetails")
@RequestScoped
public class FHIRPatientDetailsBean implements Serializable {
    private String name;
    private String lastName;
    private Date birthday;

    private Date measurementAt;
    private Float temperature;
    private String id;

    private List<BodyTemperatureMeasurementsDTOOut> temperatureList = new ArrayList<>();

    @Inject
    private HttpServletRequest request;

    @Inject
    private IFHIRPatientClient ifhirPatientClient;

    @Inject
    private IFHIRObservationClient ifhirObservationClient;

    private Patient patient;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public Date getMeasurementAt() {
        return measurementAt;
    }

    public void setMeasurementAt(Date measurementAt) {
        this.measurementAt = measurementAt;
    }

    public Float getTemperature() {
        return temperature;
    }

    public void setTemperature(Float temperature) {
        this.temperature = temperature;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<BodyTemperatureMeasurementsDTOOut> getTemperatureList() {
        return temperatureList;
    }

    public void setTemperatureList(List<BodyTemperatureMeasurementsDTOOut> temperatureList) {
        this.temperatureList = temperatureList;
    }

    @PostConstruct
    public void init()  {
        id = request.getParameter("id");
        try {
            patient = ifhirPatientClient.getPatientById(new IdType(Long.parseLong(id)));
        } catch (ResourceNotFoundException e){
            String uri = "404.html";
            try {
                FacesContext.getCurrentInstance().getExternalContext().dispatch(uri);
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        if (patient!= null) {

            setName(patient.getName().get(0).getGiven().get(0).getValue());
            setLastName(patient.getName().get(1).getFamily());
            setBirthday(patient.getBirthDate());

            List<Observation> observations = ifhirObservationClient.getObservationsForPatient(new StringType(id));
            temperatureList.clear();

            observations.stream().forEach(observation -> {
                BodyTemperatureMeasurementsDTOOut bodyTemperatureMeasurementsDTOOut = new BodyTemperatureMeasurementsDTOOut();
                bodyTemperatureMeasurementsDTOOut.setTemperature(((Quantity) observation.getValue()).getValue().floatValue());
                bodyTemperatureMeasurementsDTOOut.setMeasurementAt(new Timestamp(((DateTimeType) observation.getEffective()).getValue().getTime()));
                temperatureList.add(bodyTemperatureMeasurementsDTOOut);
            });
        }
    }

    public void addTemperature() {
        Observation observation = new Observation();

        Quantity quantity = new Quantity();
        quantity.setValue(new java.math.BigDecimal(getTemperature().toString()));
        quantity.setUnit("Cel");
        observation.setValue(quantity);
        observation.setSubject(new Reference(patient));
        observation.setEffective(new DateTimeType(getMeasurementAt()));
        Coding coding = new Coding("http://loinc.org","8310-5","Body temperature");

        observation.setCode((new CodeableConcept()).addCoding(coding));

        ifhirObservationClient.createObservation(observation);

        init();
    }

}
