package com.parsek.bean;

import com.parsek.DTO.out.ListMaxTemperaturePerPatientsDTOOut;
import com.parsek.rest.RestClientImpl;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
@Named("MaxTemperatures")
@RequestScoped
public class MaxTemperaturesBean  implements Serializable {
    private String json;

    @Inject
    private RestClientImpl restClientImpl;

    public String getJson() {
        return json;
    }

    public void setJson(String json) {
        this.json = json;
    }

    @PostConstruct
    public void init(){
        ListMaxTemperaturePerPatientsDTOOut listMaxTemperaturePerPatientsDTOOut = restClientImpl.getMaxTemperatures();
        JSONArray jsonArray = new JSONArray();
        listMaxTemperaturePerPatientsDTOOut.getPatientsMAxTemperature().stream().forEach(maxTemperaturePatientDTOOut -> {
            JSONObject jsonObject = new JSONObject();
            jsonArray.add(jsonObject);

            jsonObject.put("maxTemperature", maxTemperaturePatientDTOOut.getMaxTemperature());
            jsonObject.put("name", maxTemperaturePatientDTOOut.getName());
        });
        json = jsonArray.toJSONString();
    }
}
