package com.parsek.rest;

import com.parsek.DTO.out.ListMaxTemperaturePerPatientsDTOOut;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;

@RequestScoped
public class RestClientImpl {

    @Inject
    private IRestPatient iRestPatient;

    public ListMaxTemperaturePerPatientsDTOOut getMaxTemperatures(){
        return iRestPatient.getMaxTemperature().readEntity(ListMaxTemperaturePerPatientsDTOOut.class);
    }
}
