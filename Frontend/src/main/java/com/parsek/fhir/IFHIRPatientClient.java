package com.parsek.fhir;

import ca.uhn.fhir.rest.client.api.IBasicClient;

public interface IFHIRPatientClient extends IFHIRPatient, IBasicClient {
}
