package com.parsek.config;

import ca.uhn.fhir.context.FhirContext;
import com.parsek.fhir.IFHIRObservationClient;
import com.parsek.fhir.IFHIRPatientClient;

import javax.annotation.PostConstruct;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import java.util.Properties;


@ApplicationScoped
public class FHIRConfig {

    private String serverBase;

    @Inject
    private Properties properties;

    @PostConstruct
    public void init(){
        serverBase = properties.getProperty("fhir.server");
    }

    @Produces
    public IFHIRPatientClient getIfhirClient() {
        FhirContext fhirContext = FhirContext.forR4();
        IFHIRPatientClient ifhirPatientClient = fhirContext.newRestfulClient(IFHIRPatientClient.class, serverBase);
        return ifhirPatientClient;
    }

    @Produces
    public IFHIRObservationClient getIfhirObservation() {
        FhirContext fhirContext = FhirContext.forR4();
        IFHIRObservationClient ifhirObservation = fhirContext.newRestfulClient(IFHIRObservationClient.class, serverBase);
        return ifhirObservation;
    }


}
