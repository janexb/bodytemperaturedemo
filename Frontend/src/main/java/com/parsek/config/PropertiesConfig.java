package com.parsek.config;

import javax.ejb.Startup;
import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.context.Dependent;
import javax.enterprise.inject.Produces;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@ApplicationScoped
@Startup
public class PropertiesConfig {

    @Produces
    public Properties getProperties() {
        InputStream inputStream = this.getClass().getClassLoader().getResourceAsStream("config.properties");

        Properties properties = new Properties();

        try {
            properties.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return properties;
    }


}
