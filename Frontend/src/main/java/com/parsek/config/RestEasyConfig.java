package com.parsek.config;

import com.parsek.rest.IRestPatient;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import java.util.Properties;

@RequestScoped
public class RestEasyConfig {

    private String path;

    @Inject
    private Properties properties;

    @PostConstruct
    public void init(){
        path = properties.getProperty("rest.url");
    }

    @Produces
    private IRestPatient getIRestPatient(){

        Client client = ClientBuilder.newClient();
        WebTarget target = client.target(path);
        ResteasyWebTarget rtarget = (ResteasyWebTarget)target;

        IRestPatient proxy = rtarget.proxy(IRestPatient.class);
        return proxy;
    }
}
